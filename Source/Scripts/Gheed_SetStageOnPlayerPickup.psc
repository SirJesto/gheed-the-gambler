ScriptName Gheed_SetStageOnPlayerPickUp Extends ObjectReference

Actor Property PlayerREF Auto
Quest property QuestToSet Auto
Int Property iStageToSet Auto

Event OnContainerChanged(ObjectReference akNewContainer, ObjectReference akOldContainer)
	If akNewContainer == PlayerREF
		QuestToSet.SetStage(iStageToSet)
		GoToState("Taken")
	EndIf
EndEvent

State Taken
	; Do nothing
EndState