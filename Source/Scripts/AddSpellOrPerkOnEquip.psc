Scriptname AddSpellOrPerkOnEquip extends ObjectReference  
{Adds specifc spell(s) or perk(s) on equipping the weapon or armor item.}

Spell[] Property Spells Auto
{Any array of all spells added}
Perk[] Property Perks Auto
{Any array of all perks added}
Bool Property bSilentAdd = FALSE Auto
{Add spells silently, default = FALSE}
Actor akWearer

Event OnEquipped(Actor akActor)
	AddBuffs(akActor)
EndEvent

Event OnUnequipped(Actor akActor)
	RemoveBuffs(akActor)
EndEvent

Event OnContainerChanged(ObjectReference akNewCont, ObjectReference akOldCont)
	if akWearer
		RemoveBuffs(akWearer)	
	endif
EndEvent

Function RemoveBuffs(Actor akActor)
	if !akActor
		debug.trace("Removebuffs cancelled.")
		Return
	endif
	
	if Spells.Length == 1
		akActor.RemoveSpell(Spells[0])
	elseif Spells.Length > 1
		int index = Spells.Length
		int s
		while s < index
			if Spells[s] != None
				akActor.RemoveSpell(Spells[s])
			endif
			s += 1
		endwhile
	endif
	
	if Perks.Length == 1
		akActor.RemovePerk(Perks[0])
	elseif Perks.Length > 1
		int index = Perks.Length
		int s
		while s < index
			if Perks[s] != None
				akActor.RemovePerk(Perks[s])
			endif
			s += 1
		endwhile
	endif
	
	akWearer = None
EndFunction

Function AddBuffs(Actor akActor)
	if !akActor
		debug.trace("AddBuffs cancelled.")
		Return
	endif
	akWearer = akActor
	if Spells.Length == 1 && akActor
		akActor.AddSpell(Spells[0], !bSilentAdd)
	elseif Spells.Length > 1
		int index = Spells.Length
		int s
		while s < index
			if Spells[s] != None
				akActor.AddSpell(Spells[s], !bSilentAdd)
			endif
			s += 1
		endwhile
	endif
	
	if Perks.Length == 1
		akActor.AddPerk(Perks[0])
	elseif Perks.Length > 1
		int index = Perks.Length
		int s
		while s < index
			if Perks[s] != None
				akActor.AddPerk(Perks[s])
			endif
			s += 1
		endwhile
	endif	
EndFunction
