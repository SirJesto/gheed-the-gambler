;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 15
Scriptname QF_GSQ01Gheed_05005A30 Extends Quest Hidden

;BEGIN ALIAS PROPERTY Player
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Player Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Charm
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Charm Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY HouseKey
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_HouseKey Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY GheedHomeMarker
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_GheedHomeMarker Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY xMarker01
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_xMarker01 Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY BFExteriorMapMarker
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_BFExteriorMapMarker Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Gheed
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Gheed Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Gambler
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Gambler Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_4
Function Fragment_4()
;BEGIN CODE
SetObjectiveDisplayed(20)
SetObjectiveCompleted(10)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_12
Function Fragment_12()
;BEGIN CODE
SetObjectiveCompleted(40)
Game.GetPlayer().RemoveItem(Alias_charm.GetRef())
Game.GetPlayer().AddItem(Alias_housekey.GetRef())
Alias_GheedHomeMarker.GetRef().addtomap()
alias_Gambler.GetREF().enable(true)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_7
Function Fragment_7()
;BEGIN CODE
setObjectiveDisplayed(30)
SetObjectiveCompleted(20)
alias_Charm.GetREF().enable(true)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN CODE
Alias_BFExteriorMapMarker.GetRef().addtomap()
alias_xMarker01.GetRef().disable()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_9
Function Fragment_9()
;BEGIN CODE
SetObjectiveDisplayed(40)
SetObjectiveCompleted(30)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
