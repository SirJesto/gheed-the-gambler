scriptName EnchDetectLifeScript extends ActiveMagicEffect

;-- Properties --------------------------------------
spell property sDetectLifeSpell auto
actor property akCasterRef auto hidden

;-- Variables ---------------------------------------

;-- Functions ---------------------------------------

function OnUpdate()

	sDetectLifeSpell.Cast(akCasterRef as objectreference, none)
endFunction

; Skipped compiler generated GotoState

; Skipped compiler generated GetState

function OnEffectStart(actor akTarget, actor akCaster)

	akCasterRef = akCaster
	sDetectLifeSpell.Cast(akCasterRef as objectreference, none)
	self.RegisterForUpdate(2 as Float)
endFunction

function OnEffectFinish(actor akTarget, actor akCaster)

	self.UnregisterForUpdate()
	akCasterRef.DispelSpell(sDetectLifeSpell)
endFunction
