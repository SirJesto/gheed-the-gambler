Scriptname GheedGambleWeaponScript extends ObjectReference  

LeveledItem Property RecievedObject Auto
Weapon Property SoldObject Auto

Event OnContainerChanged(ObjectReference akNewContainer, ObjectReference akOldContainer)
    if akNewContainer == Game.GetPlayer()
        Game.GetPlayer().AddItem(RecievedObject, 1, true)
		Utility.wait(0.5)
        Game.GetPlayer().RemoveItem(SoldObject, 1, true)
    endif
EndEvent
